# 关于后台数据的更正

因为重新获取后台数据，域名有所改变所以在src/config/index.js文件中
```javascript
export default {
    // 开发打包是默认的两个环境  'http://localhost:3000'改成'http://vue.zhufengpeixun.cn'
    baseURL: process.env.NODE_ENV === 'development'? 'http://vue.zhufengpeixun.cn':'/'
}

console.log(process.env)
```
