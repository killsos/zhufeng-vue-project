![0603](https://images.gitee.com/uploads/images/2020/0607/213608_e16b33cf_1720749.png)<br />需要对manager文件夹里的文件进行动态路由的配置，所以在src/router文件夹里新建per.js文件<br />src/router/per.js文件  ---- 登录成功之后的所对应的菜单 -- 二级路由 --  /manager/userStatistics
```javascript
export default [{
        path: 'userStatistics',
        meta: {
            auth: 'userStatistics'
        },
        name: 'userStatistics',
        component: () => import( /*webpackChunkName:'manager'*/ '@/views/manager/userStatistics.vue')
    },
    {
        path: 'userAuth',
        meta: {
            auth: 'userAuth'
        },
        name: 'userAuth',
        component: () => import( /*webpackChunkName:'manager'*/ '@/views/manager/userAuth.vue')
    },
    {
        path: 'infoPublish',
        meta: {
            auth: 'infoPublish'
        },
        name: 'infoPublish',
        component: () => import( /*webpackChunkName:'manager'*/ '@/views/manager/infoPublish.vue')
    },
    {
        path: 'articleManager',
        meta: {
            auth: 'articleManager'
        },
        name: 'articleManager',
        component: () => import( /*webpackChunkName:'manager'*/ '@/views/manager/articleManager.vue')
    },
    {
        path: 'personal',
        name: 'personal',
        meta: {
            auth: 'personal'
        },
        component: () => import( /*webpackChunkName:'manager'*/ '@/views/manager/personal.vue')
    },
    {
        path: 'myCollection',
        meta: {
            auth: 'myCollection'
        },
        name: 'myCollection',
        component: () => import( /*webpackChunkName:'manager'*/ '@/views/manager/myCollection.vue')
    },
    {
        path: 'privateMessage',
        meta: {
            auth: 'privateMessage'
        },
        name: 'privateMessage',
        component: () => import( /*webpackChunkName:'manager'*/ '@/views/manager/privateMessage.vue')
    },
    {
        path: 'myArticle',
        meta: {
            auth: 'myArticle'
        },
        name: 'myArticle',
        component: () => import( /*webpackChunkName:'manager'*/ '@/views/manager/myArticle.vue')
    }
]
```
继续在src/store/action-types.js继续添加方法
```javascript
// 设置路由权限【菜单权限】
export const SET_MENU_PERMISSION = 'SET_MENU_PERMISSION';
// 添加路由动作
export const ADD_ROUTE = 'ADD_ROUTE'
```
进行判断是否添加过路由，所以在src/store/modules/user.js里进行添加
```javascript
import * as user from '@/api/user';
import * as types from '../action-types';
import { setLocal, getLocal } from '@/utils/local';
import router from '@/router'
import per from '@/router/per.js'
const filterRouter = (authList)=>{//过滤路由的方法
  //conosle.log(authList)//后端返回的数据中auth属性是进行权限鉴定的 
    let auths = authList.map(auth=>auth.auth);//获取到auths的名称进行筛选
    function filter(routes){
        return routes.filter(route=>{
            if(auths.includes(route.meta.auth)){//auths匹配上的就会有相对应的菜单
                if(route.children){//如果有娃儿的话就继续匹配
                    route.children = filter(route.children )
                }
                return route;
            }
        })
    }
    return filter(per)
}
export default {
    state: {
        userInfo: {}, // 用户信息
        hasPermission: false, // 代表用户权限
        menuPermission:false,//代表着菜单权限
    },
    mutations: {
//...
        [types.SET_MENU_PERMISSION](state, has) {
            state.menuPermission = has;
        }
    },
    actions: {
      //...
        async [types.ADD_ROUTE]({commit,state}){
            // 后端返回的用户的权限
            let authList = state.userInfo.authList; 
            //console.log(authList,per)//authList代表着后端返回的数据，per是前端设置的路径
            if(authList){ // 通过权限过滤出当前用户的路由--不同的用户有不同的菜单
                let routes = filterRouter(authList); 
                // 找到manager路由作为第一级 如/manager/articleManager.vue
               //在路由的配置信息上进行查找 --- manager这一条
                let route = router.options.routes.find(item=>item.path === '/manager');
                route.children = routes; // 给manager添加儿子路由
                router.addRoutes([route]); // 动态添加进去
                commit(types.SET_MENU_PERMISSION,true); // 权限设置完毕
            }else{
                commit(types.SET_MENU_PERMISSION,true); // 权限设置完毕
            }
        }
    }
}
```
所以在路由钩子函数hooks上进行操作<br />src/router/hooks.js文件
```javascript
import store from '../store';
import * as types from '../store/action-types'
// 登录权限校验
//...
// 路由权限动态添加
export const menuPermisson = async function(to, from, next) {
    if (store.state.user.hasPermission) {
        // 添加路由这里需要判断是否添加过路由了 
        if (!store.state.user.menuPermission) {
            // 添加路由完成，但是是属于异步加载，不会立即更新
            store.dispatch(`user/${types.ADD_ROUTE}`);
           //replace 相当于replaceState替换掉并不进入历史记录 把路径添加完全
            next({ ...to, replace: true }); // 如果是next()时，进入到页面时报404.
        } else {
            next();
        }
    } else {
        next();
    }
}
export default {
    loginPermission,
    menuPermisson,
}
```
